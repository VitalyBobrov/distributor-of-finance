import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {CreateMeetingModalComponent} from "./create-meeting-modal/create-meeting-modal.component";
import {StorageService} from "../../core/services/storage.service";
import {StartInfoModalComponent} from "./start-info-modal/start-info-modal.component";
import {StartConfirmingModalComponent} from "./start-confirming-modal/start-confirming-modal.component";
import {CreateFriendModalComponent} from "./create-friend-modal/create-friend-modal.component";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  constructor(public dialog: MatDialog,
              private storageService: StorageService) { }

  ngOnInit(): void {

    if (!this.storageService.localStorageRead('user')) {
      const startInfoModalDialogRef = this.dialog.open(StartInfoModalComponent);
      startInfoModalDialogRef.afterClosed().subscribe((userInfo) => {
        console.log('The StartInfoModalComponent was closed');
        console.log(userInfo);
        this.storageService.localStorageWrite('user', userInfo)
      });
    } else {
      const startConfirmingModalDialogRef = this.dialog.open(StartConfirmingModalComponent);
      startConfirmingModalDialogRef.afterClosed().subscribe(() => {
        console.log('The StartConfirmingModalComponent was closed');
      })
    }

  }

  openCreateMeetingModal() {
    const createMeetingModalDialogRef = this.dialog.open(CreateMeetingModalComponent, {
    });

    createMeetingModalDialogRef.afterClosed().subscribe((meetingData) => {
      console.log('The CreateMeetingModalComponent was closed', meetingData);
    });
  }

  openCreateFriendModal() {
    const createFriendModalDialogRef = this.dialog.open(CreateFriendModalComponent)
    createFriendModalDialogRef.afterClosed().subscribe(friendData => {
      console.log('The CreateFriendModalComponent was closed', friendData);
    })
  }
}
