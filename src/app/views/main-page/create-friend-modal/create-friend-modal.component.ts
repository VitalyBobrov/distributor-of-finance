import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {randomInteger} from "../../../shared/functions/functions";

@Component({
  selector: 'app-create-friend-modal',
  templateUrl: './create-friend-modal.component.html',
  styleUrls: ['./create-friend-modal.component.scss']
})
export class CreateFriendModalComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CreateFriendModalComponent>,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: null,
      phone: null,
      id: randomInteger(0, 1000000)
    })
  }

  saveAndSubmit() {
    this.dialogRef.close(this.form.value);
  }
}
