import {Component, Inject, OnInit} from '@angular/core';
import { randomInteger } from "../../../shared/functions/functions";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-start-info-modal',
  templateUrl: './start-info-modal.component.html',
  styleUrls: ['./start-info-modal.component.scss']
})
export class StartInfoModalComponent implements OnInit {
  userIcon: string;
  iconNumber: number;
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<StartInfoModalComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.form = this.fb.group({
      name: '',
      phone: '',
      id: null,
      icon: ''
    });
  }

  ngOnInit(): void {
    this.changeUserIcon();
    this.form.get('id').setValue(randomInteger(0, 1000000));
    this.form.valueChanges.subscribe(item => console.log(item));
  }

  changeUserIcon() {
    this.userIcon = `./assets/icons/users-avatars/Artboard${randomInteger(9,44)}.svg`;
    this.form.get('icon').setValue(this.userIcon);
  }

  saveAndSubmit() {
    this.dialogRef.close(this.form.value);
  }
}
