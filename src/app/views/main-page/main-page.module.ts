import {NgModule} from "@angular/core";
import {MainPageComponent} from "./main-page.component";
import {MatTabsModule} from "@angular/material/tabs";
import { MeetingCardsComponent } from './meeting-cards/meeting-cards.component';
import { MyFriendsComponent } from './my-friends/my-friends.component';
import {RouterModule, Routes} from "@angular/router";
import { CreateMeetingModalComponent } from './create-meeting-modal/create-meeting-modal.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatRadioModule} from "@angular/material/radio";
import {ReactiveFormsModule} from "@angular/forms";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {CommonModule} from "@angular/common";
import { StartInfoModalComponent } from './start-info-modal/start-info-modal.component';
import { StartConfirmingModalComponent } from './start-confirming-modal/start-confirming-modal.component';
import {MatListModule} from "@angular/material/list";
import { CreateFriendModalComponent } from './create-friend-modal/create-friend-modal.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent
  }
]

@NgModule({
  imports: [
    MatTabsModule,
    MatDialogModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    CommonModule,
    MatListModule,
  ],
  declarations: [MainPageComponent, MeetingCardsComponent, MyFriendsComponent, CreateMeetingModalComponent, StartInfoModalComponent, StartConfirmingModalComponent, CreateFriendModalComponent],
  exports: [RouterModule]
})
export class MainPageModule {}
