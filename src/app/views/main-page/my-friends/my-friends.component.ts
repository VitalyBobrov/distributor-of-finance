import {Component, Input, OnInit} from '@angular/core';
import {IMember} from "../../../shared/models/interfaces";

@Component({
  selector: 'app-my-friends',
  templateUrl: './my-friends.component.html',
  styleUrls: ['./my-friends.component.scss']
})
export class MyFriendsComponent implements OnInit {
  @Input() friends: IMember[];

  constructor() { }

  ngOnInit(): void {
  }

}
