import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {IMeeting, IMember} from "../../../shared/models/interfaces";
import {map} from "rxjs/operators";
import {StorageService} from "../../../core/services/storage.service";
import {MatOptionSelectionChange} from "@angular/material/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {randomInteger} from "../../../shared/functions/functions";

@Component({
  selector: 'app-create-meeting-modal',
  templateUrl: './create-meeting-modal.component.html',
  styleUrls: ['./create-meeting-modal.component.scss']
})
export class CreateMeetingModalComponent implements OnInit {
  form: FormGroup;
  members: IMember[];

  filteredMembers: Observable<IMember[]>;
  currentMeetingMembers: IMember[];

  constructor(private fb: FormBuilder,
              private storageService: StorageService,
              private dialogRef: MatDialogRef<CreateMeetingModalComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
    this.form = fb.group({
      name: null,
      id: randomInteger(0, 1000000),
      member: null,
    })
  }

  ngOnInit(): void {
    this.members = this.storageService.localStorageRead('members');
    this.form.get('id').setValue(randomInteger(0,1000000));
    this.filteredMembers = this.form.get('member').valueChanges.pipe(
      map(value => this.members.filter(member => member.name.includes(value)))
    )
  }

  pushNewMember(event: MatOptionSelectionChange) {
    this.currentMeetingMembers.push(event.source.value);
  }

  saveAndSubmit() {
    this.dialogRef.close({formValue: this.form.value, members: this.currentMeetingMembers});
  }
}
