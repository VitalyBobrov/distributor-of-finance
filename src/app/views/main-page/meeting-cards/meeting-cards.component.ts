import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meeting-cards',
  templateUrl: './meeting-cards.component.html',
  styleUrls: ['./meeting-cards.component.scss']
})
export class MeetingCardsComponent implements OnInit {
  cards = [1,2,3,4,5]

  constructor() { }

  ngOnInit(): void {
  }

}
