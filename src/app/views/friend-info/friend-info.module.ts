import { CommonModule } from "@angular/common";
import {NgModule} from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatRadioModule } from "@angular/material/radio";
import {RouterModule, Routes} from "@angular/router";
import {FriendInfoComponent} from "./friend-info.component";

const routes: Routes = [
  {
    path: '',
    component: FriendInfoComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes), MatExpansionModule, MatIconModule, MatFormFieldModule, MatInputModule, MatListModule, MatButtonModule, MatMenuModule, MatRadioModule, CommonModule],
  declarations: [FriendInfoComponent],
  exports: [RouterModule]
})
export class FriendInfoModule {}
