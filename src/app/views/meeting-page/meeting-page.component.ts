import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {CalculatedModalComponent} from "./calculated-modal/calculated-modal.component";

@Component({
  selector: 'app-meeting-page',
  templateUrl: './meeting-page.component.html',
  styleUrls: ['./meeting-page.component.scss']
})
export class MeetingPageComponent implements OnInit {
  friends = [
    {
      name: "Ildar"
    },
    {
      name: "Ildar"
    },
    {
      name: "Ildar"
    },
    {
      name: "Ildar"
    },
    {
      name: "Ildar"
    },

  ]

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openCalculatedModal() {
    const calculatedModalDialogRef = this.dialog.open(CalculatedModalComponent);
    calculatedModalDialogRef.afterClosed().subscribe(() => {
      console.log('The CalculatedModalComponent was closed');
    });
  }
}
