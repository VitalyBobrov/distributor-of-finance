import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-calculated-modal',
  templateUrl: './calculated-modal.component.html',
  styleUrls: ['./calculated-modal.component.scss']
})
export class CalculatedModalComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<CalculatedModalComponent>) { }

  ngOnInit(): void {
  }

}
