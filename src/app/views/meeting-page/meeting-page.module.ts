import {NgModule} from "@angular/core";
import {MeetingPageComponent} from "./meeting-page.component";
import {RouterModule, Routes} from "@angular/router";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatListModule } from "@angular/material/list";
import {CommonModule} from "@angular/common";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatRadioModule} from "@angular/material/radio";
import { CalculatedModalComponent } from './calculated-modal/calculated-modal.component';
import {MatDialogModule} from "@angular/material/dialog";

const routes: Routes = [
  {
    path: '',
    component: MeetingPageComponent
  }
]

@NgModule({
  imports: [MatExpansionModule, MatDialogModule, MatIconModule, MatFormFieldModule, MatInputModule, MatListModule, MatButtonModule, MatMenuModule, MatRadioModule, CommonModule, RouterModule.forChild(routes)],
  declarations: [MeetingPageComponent, CalculatedModalComponent],
  exports: [RouterModule]
})
export class MeetingPageModule {}
