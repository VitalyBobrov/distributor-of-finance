import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import('./views/main-page/main-page.module').then(m => m.MainPageModule)
  },
  {
    path: 'meeting',
    loadChildren: () => import('./views/meeting-page/meeting-page.module').then(m => m.MeetingPageModule)
  },
  {
    path: 'friend-info',
    loadChildren: () => import('./views/friend-info/friend-info.module').then(m => m.FriendInfoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
