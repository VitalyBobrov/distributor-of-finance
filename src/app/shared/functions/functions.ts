import {IMember} from "../models/interfaces";

export function setMemberId(member: IMember) {
  member.id = randomInteger(0, 1000000)
  return member;
}

export function randomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}
