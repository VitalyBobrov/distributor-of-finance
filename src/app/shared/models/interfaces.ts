export interface IMember {
  id?: number;
  name: string
  phone?: string;
  age?: number;
  description?: string;
}

export interface IMeeting {
  id? : number;
  name?: string;
  members?: IMember[];
  bankMember?: IMember;
}
