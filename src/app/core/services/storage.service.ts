import { Injectable, NgZone } from '@angular/core';

declare const unescape: any;
declare const escape: any;

@Injectable({providedIn: "root"})
export class StorageService {
  constructor(private zone: NgZone) {}

  public sessionStorageWrite(key, value) {
    window.sessionStorage.setItem(
      this.encodeValue(key),
      this.encodeValue(this.serialize(value)),
    );
  }

  public localStorageWrite(key, value) {
    console.log(this.encodeValue(this.serialize(value)));
    window.localStorage.setItem(
      this.encodeValue(key),
      this.encodeValue(this.serialize(value)),
    );
  }

  public sessionStorageRead(key) {
    return this.zone.run(() => {
      const value = window.sessionStorage.getItem(this.encodeValue(key));
      if (value === null) {
        return value;
      }
      return this.deserialize(this.decodeValue(value));
    });
  }

  public localStorageRead(key) {
    return this.zone.run(() => {
      const value = window.localStorage.getItem(this.encodeValue(key));
      if (value === null) {
        return value;
      }
      return this.deserialize(this.decodeValue(value));
    });
  }

  private encodeValue(value) {
    return btoa(escape(value));
  }

  private decodeValue(value) {
    return unescape(atob(value));
  }

  public sessionStorageRemove(key) {
    window.sessionStorage.removeItem(this.encodeValue(key));
  }

  public localStorageRemove(key) {
    window.localStorage.removeItem(this.encodeValue(key));
  }

  private serialize(data): string {
    if (typeof data === 'undefined') {
      throw new Error(
        'Storage service serialization error: Unable to store undefined value!',
      );
    }
    if (typeof data !== 'string') {
      try {
        return JSON.stringify(data);
      } catch (_) {
        console.error(data);
        throw new Error(
          'Storage service serialization error: Unable to serialize that value!',
        );
      }
    }
    return data;
  }

  private deserialize(data): any {
    try {
      return JSON.parse(data);
    } catch (_) {
      console.error(data);
      throw new Error(
        'Storage service deserialization error: Unable to parse stored data!',
      );
    }
  }
}
