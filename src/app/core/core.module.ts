import {NgModule} from "@angular/core";
import {HeaderComponent} from "./header/header.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

@NgModule({
  imports: [MatExpansionModule, MatIconModule, MatFormFieldModule, MatInputModule],
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent
  ]
})
export class CoreModule {}
