function eventSlider() {
  if (document.querySelector('.slider-meeting')) {
    const flkty = new Flickity('.slider-meeting__wrap', {
      freeScroll: true,
      contain: true,
      prevNextButtons: false,
      pageDots: false
    })
    const eventStatus = document.querySelector('.slider-meeting__status');

    function updateStatus() {
      let slideNumber = flkty.selectedIndex + 1;
      eventStatus.textContent = slideNumber + '/' + flkty.slides.length;
    }

    updateStatus()

    flkty.on('select', updateStatus);
  }



}

window.onload = function () {
  eventSlider();
}
